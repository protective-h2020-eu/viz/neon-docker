FROM tomcat:8.0.50-jre8

RUN mkdir /usr/local/tomcat/webapps/myapp

COPY ./neon-war/neon-1.3.0.war /usr/local/tomcat/webapps/neon.war
